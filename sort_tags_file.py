from PIL import Image


def sort_tags(src_tags_file, sorted_tags_file):
    tags_width = {} 
    with open(src_tags_file) as fo:
        for line_id, line in enumerate(fo):
            image_path = line.split(' ')[0]
            width, height = Image.open(image_path).convert('L').size
            new_width = (width / height) * 32
            tags_width[line] = new_width
            if line_id % 1000 == 0:
                print(line_id)
    print('read tags finshed, begin sort...')

    with open(sorted_tags_file, 'w') as fo:
        sorted_tags_width = sorted(tags_width.items(), key=lambda e: e[1])
        for (line, w) in sorted_tags_width:
            fo.write(line)

    print('finished!')


def test_sort_tags():
    src_tags_file = './word_vert.tags'
    sorted_tags_file = './word_vert_sorted.tags'
    sort_tags(src_tags_file, sorted_tags_file)


if __name__ == '__main__':
    test_sort_tags()

