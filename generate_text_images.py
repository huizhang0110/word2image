#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont
import cv2
import numpy as np
import os
import pickle
from os.path import dirname
import math
from os.path import join, isdir, realpath
from os import makedirs
import codecs
import random


data_root_dir = os.path.dirname(os.path.abspath(__file__))
font_dir = '/home/zhui/datasets/fonts/'
font_cache = os.path.join(font_dir, "fonts.pkl")

font_un = ['Kaiti.ttf', 'SimSun.ttf', 'SimHei.ttf', 'PMingLiU.ttf']

def add_salt_pepper(img, fill=(255, 255, 255), num_ratio=None):
    """
    Args:
        img: PIL.Image type
        fill: noise color, tuple of length 3, default: Write color
        num: noise spot number, int 
    Return:
        img: PIL.Image type
    """
    if num_ratio is None:
        num_ratio = np.random.uniform(0.06, 0.15)
    num = int(img.size[0] * img.size[1] * num_ratio)
    pix = img.load()
    for k in range(num):
        x = int(np.random.uniform(0, img.size[0]))
        y = int(np.random.uniform(0, img.size[1]))
        r, g, b = pix[x, y]
        img.putpixel([x, y], fill)
    return img


def refresh_fonts_cache():
    import glob
    from PIL import ImageFont
    en_fonts = []
    unicode_fonts = []
    for file in glob.glob(font_dir + "/*.ttf"):
        font = ImageFont.truetype(file, 14)
        name = os.path.basename(file)
        glyphs = font.font.glyphs
        print("name: %s, glyphs: %d, style: %s" % (font.font.family, glyphs, font.font.style))
        if glyphs > 10000:
            unicode_fonts.append(name)
        else:
            en_fonts.append(name)
    with open(font_cache, 'wb') as output:
        pickle.dump((en_fonts, unicode_fonts), output, pickle.HIGHEST_PROTOCOL)
    return en_fonts, unicode_fonts


if os.path.isfile(font_cache):
    with open(font_cache, 'rb') as f:
        en_fonts, unicode_fonts = pickle.load(f)
        print('fonts.pkl load down!')
else:
    en_fonts, unicode_fonts = refresh_fonts_cache()
    print('no pkl load')
all_fonts = en_fonts + unicode_fonts
# print('font:',unicode_fonts)

def has_unicode(text):
    for t in text:
        if ord(t) > 255:
            return True
    else:
        return False


def get_font(text, font_size, font_name=None, multi_fonts=False):
    if font_name is None and multi_fonts:
        if has_unicode(text):
            fonts = unicode_fonts
        else:
            fonts = all_fonts
        font_name = np.random.choice(fonts)
        while font_name == 'symbol.ttf':
            font_name = np.random.choice(fonts)
    else:
        font_name = unicode_fonts[0]
    font_file = os.path.join(font_dir, font_name)
    # return ImageFont.truetype(font_file, font_size, encoding='unic')
    return font_file


def split_text(text):
    if text is None:
        return []
    if len(text) == 1:
        return [(has_unicode(text), text)]
    start = None
    current_is_unicode = None
    texts = []
    for i in range(len(text)):
        is_unicode = ord(text[i]) > 255
        if current_is_unicode is None:
            current_is_unicode = is_unicode
            start = i
        elif current_is_unicode == is_unicode:
            pass
        else:
            texts.append((current_is_unicode, text[start:i]))
            start = i
            current_is_unicode = is_unicode
        if i == len(text)-1:
            texts.append((current_is_unicode, text[start:]))
    return texts
symbol_chn = ['℃','Ⅰ','Ⅱ','Ⅲ','Ⅳ','Ⅴ','Ⅵ','Ⅶ','Ⅷ','Ⅸ','Ⅹ','Ⅺ','Ⅻ','、',
'〃','〈','〉','《','》','【','】','＂','＃','＄','％','＆','＇','（','）','＊',
'＋','，','－','：','；','＜','＝','＞','？','^','～','°','￡','￥']
def need_rotate(text):
    if text is None:
        return []
    if len(text) == 1:
        if ord(text) < 255 or text in symbol_chn:
            return [(False,text)]
        else:
            return [(True,text)]
    start = None
    current_is_unicode = None
    texts = []
    for i in range(len(text)):
        is_unicode = ord(text[i]) > 255 and text[i] not in symbol_chn
        if current_is_unicode is None:
            current_is_unicode = is_unicode
            start = i
        elif current_is_unicode == is_unicode:
            pass
        else:
            texts.append((current_is_unicode, text[start:i]))
            start = i
            current_is_unicode = is_unicode
        if i == len(text)-1:
            texts.append((current_is_unicode, text[start:]))
    return texts


def random_noise(image, mode=None):
    from skimage.util import random_noise, img_as_float, img_as_ubyte
    modes = ['gaussian', 'localvar', 'poisson', 'salt', 'pepper', 's&p', 'speckle']
    if mode is None:
        mode = np.random.choice(modes)
    image = img_as_float(image)
    image = random_noise(image, mode)
    image = img_as_ubyte(image)
    return image


def paint_vertical_text(text, size=None, font_size=None, shift=False, rotate=False, multi_fonts=False,
                        random_color=False, font_name=None):
    if font_size is None:
        font_size = np.random.randint(12, 22)
    if random_color:
        if np.random.randint(0, 100) < 5:
            bg_color_r = np.random.randint(0, 255)
            bg_color_g = np.random.randint(0, 255)
            bg_color_b = np.random.randint(0, 255)
            text_color_r = (bg_color_r + 100) % 255
            text_color_g = (bg_color_g + 100) % 255
            text_color_b = (bg_color_b + 100) % 255
        else:
            bg_color_r = bg_color_g = bg_color_b = 255
            text_color_r = text_color_g = text_color_b = np.random.randint(0, 100)
            # color = np.random.randint(0, 100)
    else:
        bg_color_r = bg_color_g = bg_color_b = 255
        text_color_r = text_color_g = text_color_b = 0
    font_name = np.random.choice(font_un)
    font_file = os.path.join(font_dir, font_name)
    # font = ImageFont.truetype(font_file, font_size, encoding='unic')
    # font_file = get_font(text, font_size, font_name, multi_fonts)
    # texts = split_text(text)
    texts = need_rotate(text)
    def text_image(text):
        font = ImageFont.truetype(font_file, font_size, encoding='unic')
        text_w, text_h = font.getsize(text)
        # image = Image.new('L', (text_w, text_h), color=bg_color)
        image = Image.new('RGB', (text_w, text_h), color=(bg_color_r, bg_color_g, bg_color_b))
        draw = ImageDraw.Draw(image)
        # 干扰线
        if np.random.randint(0, 100)<10:
            line_count = np.random.randint(1, 4)
            for i in range(line_count):
                x1 = np.random.randint(0, image.height)
                x2 = np.random.randint(0, image.height)
                y1 = np.random.randint(0, image.width)
                y2 = np.random.randint(0, image.width)
                line_width = np.random.randint(1, 5)
                fill_color_b = np.random.randint(0, 255)
                fill_color_g = np.random.randint(0, 255)
                fill_color_r = np.random.randint(0, 255)
                draw.line([(x1, y1), (x2, y2)], fill=(fill_color_b, fill_color_g, fill_color_r), width=line_width)
        # 干扰背景
        if np.random.randint(0, 100)<10:
            x1 = np.random.randint(0, image.height)
            x2 = np.random.randint(0, image.height)
            y1 = np.random.randint(0, image.width)
            y2 = np.random.randint(0, image.width)
            line_width = np.random.randint(image.height/2, image.height)
            fill_color_r = (text_color_r + 80)
            fill_color_g = (text_color_g + 80)
            fill_color_b = (text_color_b + 80)
            draw.line([(x1, y1), (x2, y2)], fill=(fill_color_b, fill_color_g, fill_color_r), width=line_width)
        draw.text((0, 0), text, font=font, fill=(text_color_r, text_color_g, text_color_b))
        return image

    word_images = []
    for is_unicode, word in texts:
        if is_unicode:
            for char in word:
                word_image = text_image(char)
                word_images.append(word_image)
        else:
            
            word_image = text_image(word)
            # print('word_image: ',word_image.size[0],word_image.size[1])
            # cv2.imshow('word_image', np.array(word_image))
            # cv2.waitKey()
            word_image = word_image.rotate(-90, expand=True)
            word_images.append(word_image)
            # return
    if word_images==[]:
        # print('not chn')
        return
    border = np.random.randint(0, 4)
    text_w = max([img.size[0] for img in word_images])
    text_h = sum([img.size[1] for img in word_images])
    w = text_w + border * 2
    h = text_h + border * 2# + np.random.randint(0, 8)
    # image = Image.new('L', (w, h), color=bg_color)
    image = Image.new('RGB', (w, h), color=(255, 255, 255))
    if shift:
        max_shift = max(0, (h - text_h) // 2)
        y = (h - text_h) // 2 + np.random.randint(-max_shift, max_shift+1)
    else:
        y = (h - text_h) // 2
    for img in word_images:
        iw, ih = img.size
        # print(iw,ih)
        # cv2.imshow('img', np.array(img))
        # cv2.waitKey()
        image.paste(img, ((w-iw)//2, y))
        y += ih
    if rotate:
        angle = np.random.randint(-3, 4)
        image = image.convert('RGBA')
        # if np.random.randint(0,100)>50:
        if 100>50:
            rot = image.rotate(90+angle, resample=Image.BICUBIC, expand=True)
        else:
            rot = image.rotate(angle, resample=Image.BICUBIC, expand=True)
        fff = Image.new('RGBA', rot.size, (255,)*4)
        # fff = Image.new('RGBA', rot.size, (bg_color, bg_color, bg_color, 255))
        # image = Image.composite(rot, fff, rot).convert("L")
        image = Image.composite(rot, fff, rot).convert("RGB")
    else:
        image = image.rotate(90, expand=True)
    return image


def paint_text(text, size=None, font_size=None, shift=False, rotate=False, multi_fonts=False,
               random_color=False, font_name=None):
    if font_size is None:
        font_size = np.random.randint(20, 21)

    font = get_font(text, font_size, font_name, multi_fonts)

    # offset_x, offset_y = font.getoffset(text)
    offset_x = 0
    offset_y = 0 #font.getmetrics()[1]
    text_w, text_h = font.getsize(text)
    if text_w == 0 or text_h == 0:
        return None
    if size is None:
        border = 0#np.random.randint(4, 8)
        w = text_w + border * 2# + np.random.randint(0, 16)
        h = text_h + border * 2
    else:
        w, h = size
    # ZHUI
    # h = 32

    if shift:
        max_shift = max(0, min(5, (w - text_w) // 2, (h - text_h) // 2))
        x = (w - text_w) // 2 + np.random.randint(-max_shift, max_shift+1)
        y = (h - text_h) // 2 + np.random.randint(-max_shift, max_shift+1)
    else:
        x = (w - text_w) // 2
        y = (h - text_h) // 2
    if random_color:
        if np.random.randint(0, 100) < 10:
            bg_color_r = np.random.randint(0, 255)
            bg_color_g = np.random.randint(0, 255)
            bg_color_b = np.random.randint(0, 255)
            text_color_r = (bg_color_r + 100) % 255
            text_color_g = (bg_color_g + 100) % 255
            text_color_b = (bg_color_b + 100) % 255
        else:
            bg_color_r = bg_color_g = bg_color_b = 255
            text_color_r = text_color_g = text_color_b = np.random.randint(0, 100)
            # color = np.random.randint(0, 100)
    else:
        bg_color_r = bg_color_g = bg_color_b = 255
        text_color_r = text_color_g = text_color_b = 0
    # image = Image.new('L', (w, h), color=bg_color)
    image = Image.new('RGB', (w, h), color=(bg_color_r, bg_color_g, bg_color_b))
    draw = ImageDraw.Draw(image)
    # 干扰线
    if len(text) > 1 and np.random.randint(0, 100)<0:
        line_count = np.random.randint(1, 4)
        for i in range(line_count):
            x1 = np.random.randint(0, image.width)
            x2 = np.random.randint(0, image.width)
            y1 = np.random.randint(0, image.height)
            y2 = np.random.randint(0, image.height)
            line_width = np.random.randint(1, 5)
            fill_color_b = np.random.randint(0, 255)
            fill_color_g = np.random.randint(0, 255)
            fill_color_r = np.random.randint(0, 255)
            draw.line([(x1, y1), (x2, y2)], fill=(fill_color_b, fill_color_g, fill_color_r), width=line_width)
    # 干扰背景
    if len(text) > 1 and np.random.randint(0, 100)<0:
        x1 = np.random.randint(0, image.width)
        x2 = np.random.randint(0, image.width)
        y1 = np.random.randint(0, image.height)
        y2 = np.random.randint(0, image.height)
        line_width = np.random.randint(image.height/2, image.height)
        fill_color_r = (text_color_r + 80)
        fill_color_g = (text_color_g + 80)
        fill_color_b = (text_color_b + 80)
        draw.line([(x1, y1), (x2, y2)], fill=(fill_color_b, fill_color_g, fill_color_r), width=line_width)
    draw.text((x - offset_x, y - offset_y), text, font=font, fill=(text_color_r, text_color_g, text_color_b))
    # 加入白色的椒盐噪声
    if np.random.random() < 0:
        add_salt_pepper(image)
    # 加入黑色的椒盐噪声
    if np.random.random() < 0:
        add_salt_pepper(image, (0, 0, 0))
    if rotate:
        angle = np.random.randint(-3, 4)
        # if np.random.randint(0, 100)<50:
        #     angle = 45
        # else:
        #     angle = -45
        image = image.convert('RGBA')
        rot = image.rotate(angle, resample=Image.BICUBIC, expand=True)
        fff = Image.new('RGBA', rot.size, (255,)*4)
        # fff = Image.new('RGBA', rot.size, (bg_color, bg_color, bg_color, 255))
        # image = Image.composite(rot, fff, rot).convert("L")
        image = Image.composite(rot, fff, rot).convert("RGB")
    cv2.imshow('img', np.array(image))
    cv2.waitKey()
    return image

def random_pad_image(image):
    max_width = image.shape[-1] + np.random.randint(1, 64)
    max_width = min(512, max_width)
    if max_width >= image.shape[-1]:
        return image
    padded_data = np.zeros((1, image.shape[-2], max_width), dtype=np.uint8)
    padded_data.fill(255)
    # x = (self.max_width - data.shape[-1])//2
    x = np.random.randint(0, max_width-image.shape[-1])
    padded_data[0, 0:image.shape[-2], x:x+image.shape[-1]] = image
    return padded_data

def normalize_image(img):
    w, h = img.size
    aspect_ratio = float(w) / float(h)
    # 图是竖直的，即长边是高，需要归一到宽为image_height
    if aspect_ratio < 1:
        img = img.resize(
            (32, int(32/aspect_ratio)),
            Image.ANTIALIAS)
        w_new, h_new = img.size
        if h_new<12:
            img = img.resize((
                32, 12),
                Image.ANTIALIAS)
        elif h_new>512:
            img = img.resize((
                32, 512),
                Image.ANTIALIAS)
    # 图水平的，即长边是宽,需要归一到高为image_height
    else:
        img = img.resize(
            (int(32*aspect_ratio), 32),
            Image.ANTIALIAS)
        w_new, h_new = img.size
        if w_new<12:
            img = img.resize(
                (12, 32),
                Image.ANTIALIAS)
        elif w_new>512:
            img = img.resize(
                (512, 32),
                Image.ANTIALIAS)
    w, h = img.size
    # if w==32 and h!=32:
    #     img = img.rotate(90, resample=Image.BICUBIC, expand=True)
    # img_bw = img.convert('L')
    img_bw = np.asarray(img, dtype=np.uint8)
    # img_bw = img_bw.transpose([2, 0, 1])
    # img_bw = img_bw[np.newaxis, :]
    return img_bw



if __name__ == "__main__":
    gen_images_dir = os.path.join(data_root_dir, 'images/vertical_imgs')
    words_file = os.path.join(data_root_dir, 'txt/word_vert.txt')
    tags_file = os.path.join(data_root_dir, 'tags/word_vert.tags')

    if not os.path.exists(gen_images_dir):
        os.mkdir(gen_images_dir)
    image_id = 1

    word_list = list()

    def read_words_file(words_file):
        with codecs.open(words_file, 'r', encoding='utf-8') as the_file:
            lines = the_file.readlines()
            for line in lines:
                line = line.strip()
                if len(line) > 0:
                    word_list.append(line)

    read_words_file(words_file)
    # word_list = sorted(list(word_list))

    print('len of word list = %d' % (len(word_list), )) # 141036

    def generate_image(tags_fobj, text, image_id):
        image = paint_vertical_text(text, size=None, shift=False, rotate=False, multi_fonts=True, random_color=True)
        if image is None:
            print("paint_text: %s failed" % text)
        image_path = join(gen_images_dir, "%d.jpg" % image_id)
        image.save(image_path)
        tags_fobj.write('%s %s\n' % (image_path, text))
        return image

    print('start generating images')
    with codecs.open(tags_file, 'w', encoding='utf-8') as the_file:
        for num_ in range(2):  # 每个word生成n张图片
            for text in word_list:
                generate_image(the_file, text, image_id)
                image_id += 1
                if image_id % 1000 == 0:
                    print('image id: ', image_id)
                # if image_id == 1000:
                #     break
    print("All finished")
